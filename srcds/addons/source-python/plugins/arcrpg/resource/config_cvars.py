from controlled_cvars import ControlledConfigManager
from controlled_cvars.handlers import (
    bool_handler, int_handler, sound_nullable_handler)

from ..info import info


CONFIG_FILE = info.basename + '/' + info.basename + '_main'

config_manager = ControlledConfigManager(CONFIG_FILE, cvar_prefix="arcrpg_")
config_manager.controlled_cvar(
    int_handler,
    name="levelup_base_exp",
    default=1000,
    description="",
)
config_manager.controlled_cvar(
    int_handler,
    name="levelup_exp_increase",
    default=500,
    description="",
)
config_manager.controlled_cvar(
    int_handler,
    name="levelup_stats_bonus",
    default=5,
    description="",
)
config_manager.controlled_cvar(
    sound_nullable_handler,
    name="levelup_sound",
    default="source-python/arcrpg/levelup.mp3",
    description="",
)
config_manager.controlled_cvar(
    bool_handler,
    name="announce_received_exp_in_chat",
    default=0,
    description="",
)
config_manager.write()
config_manager.execute()
