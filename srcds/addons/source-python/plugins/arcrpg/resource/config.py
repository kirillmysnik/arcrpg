from configparser import ConfigParser

from .paths import ARCRPG_DATA_PATH


CONFIG_FILE = ARCRPG_DATA_PATH / "config.ini"

config = ConfigParser()
config.read(CONFIG_FILE)
