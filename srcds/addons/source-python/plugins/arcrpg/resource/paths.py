from paths import GAME_PATH, PLUGIN_DATA_PATH

from ..info import info


ARCRPG_DATA_PATH = PLUGIN_DATA_PATH / info.basename
MAPDATA_PATH = GAME_PATH / "mapdata" / info.basename
