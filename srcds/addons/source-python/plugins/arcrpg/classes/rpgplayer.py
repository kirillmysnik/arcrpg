from colors import Color
from entities.entity import Entity
from listeners.tick import Delay
from mathlib import NULL_VECTOR, Vector
from messages import HudMsg, SayText2
from players.helpers import index_from_userid

from ..internal_events import InternalEvent

from ..resource.config_cvars import config_manager

from ..resource.strings import COLOR_SCHEME, strings_common, strings_popups

from .character import BotCharacter


EXP_MSG_COLOR = Color(120, 255, 138)
EXP_MSG_X = 0.01
EXP_MSG_Y = 0.01
EXP_MSG_EFFECT = 0
EXP_MSG_FADEIN = 0
EXP_MSG_FADEOUT = 0
EXP_MSG_HOLDTIME = 5
EXP_MSG_FXTIME = 0
EXP_MSG_CHANNEL = 1

QUEST_MSG_COLOR = Color(255, 255, 0)
QUEST_MSG_X = -1
QUEST_MSG_Y = 0.45
QUEST_MSG_EFFECT = 2
QUEST_MSG_FADEIN = 0.05
QUEST_MSG_FADEOUT = 0
QUEST_MSG_HOLDTIME = 3
QUEST_MSG_FXTIME = 0
QUEST_MSG_CHANNEL = 2


def vector_from_str(str_):
    return Vector(*map(float, str_.split(' ')))


def str_from_vector(vector):
    return "{x} {y} {z}".format(x=vector.x, y=vector.y, z=vector.z)


requested_mobs = []


class BaseRPGPlayer:
    def __init__(self, player, character):
        from ..modules.weapon_stats import weapon_dispenser

        self.player = player
        self.character = character
        self.weapon_dispenser = weapon_dispenser

    @property
    def is_mob(self):
        raise NotImplementedError

    def reequip(self):
        for index in self.player.weapon_indexes():
            weapon = Entity(index)
            self.player.drop_weapon(
                weapon.pointer, NULL_VECTOR, NULL_VECTOR)

            weapon.remove()

        for item_instance in (
                 self.character.equipment.all_equipped_item_instances):

            self.weapon_dispenser.create_entity_and_connect(
                self.player, item_instance)

    def store_current_pos(self):
        self.character.last_pos = {
            'origin': str_from_vector(self.player.origin),
            'angles': str_from_vector(self.player.angles),
        }

    def restore_last_pos(self):
        if self.character.last_pos is None:
            raise ValueError("Can't load last character position "
                             "as it's unknown")

        origin = vector_from_str(self.character.last_pos['origin'])
        angles = vector_from_str(self.character.last_pos['angles'])
        self.player.teleport(origin, angles, None)


class BotRPGPlayer(BaseRPGPlayer):
    is_mob = True


class RPGPlayer(BaseRPGPlayer):
    is_mob = False

    def __init__(self, player, character):
        super().__init__(player, character)

        self._active_popup = None
        self._display_exp_delay = None

    def start(self):
        self.display_exp()

    def stop(self):
        for delay in (self._display_exp_delay, ):
            if delay.running:
                delay.cancel()

    def display_exp(self):
        if (self._display_exp_delay is not None and
                self._display_exp_delay.running):

            self._display_exp_delay.cancel()

        hud_msg = HudMsg(
            strings_popups['exp_indicator'].tokenize(
                exp=self.character.exp,
                levelup_exp=self.character.levelup_exp,
                level=self.character.level,
            ),
            color1=EXP_MSG_COLOR,
            x=EXP_MSG_X,
            y=EXP_MSG_Y,
            effect=EXP_MSG_EFFECT,
            fade_in=EXP_MSG_FADEIN,
            fade_out=EXP_MSG_FADEOUT,
            hold_time=EXP_MSG_HOLDTIME,
            fx_time=EXP_MSG_FXTIME,
            channel=EXP_MSG_CHANNEL,
        )
        hud_msg.send(self.player.index)
        self._display_exp_delay = Delay(EXP_MSG_HOLDTIME, self.display_exp)

    def send_popup(self, popup):
        if self._active_popup is not None:
            self._active_popup.close()

        self._active_popup = popup
        popup.send(self.player.index)

    def receive_exp(self, amount):
        self.character.exp += int(amount)

        self.check_levelup()
        self.display_exp()

    def check_levelup(self):
        if self.character.check_levelup():
            tell(self, strings_common['levelup'], level=self.character.level)
            if config_manager['levelup_sound'] is not None:
                config_manager['levelup_sound'].play(self.player.index)

    def is_quest_started(self, quest):
        return self.character.is_quest_started(quest)

    def is_quest_completed(self, quest):
        return self.character.is_quest_completed(quest)

    def complete_quest(self, quest):
        self.character.complete_quest(quest)

        quest.complete(self)

        hud_msg = HudMsg(
            strings_popups['quest completed'].tokenize(
                title=quest.title,
            ),
            color1=QUEST_MSG_COLOR,
            x=QUEST_MSG_X,
            y=QUEST_MSG_Y,
            effect=QUEST_MSG_EFFECT,
            fade_in=QUEST_MSG_FADEIN,
            fade_out=QUEST_MSG_FADEOUT,
            hold_time=QUEST_MSG_HOLDTIME,
            fx_time=QUEST_MSG_FXTIME,
            channel=QUEST_MSG_CHANNEL,
        )
        hud_msg.send(self.player.index)

    def start_quest(self, quest):
        self.character.start_quest(quest)

        quest.start(self)

        hud_msg = HudMsg(
            strings_popups['quest started'].tokenize(
                title=quest.title,
            ),
            color1=QUEST_MSG_COLOR,
            x=QUEST_MSG_X,
            y=QUEST_MSG_Y,
            effect=QUEST_MSG_EFFECT,
            fade_in=QUEST_MSG_FADEIN,
            fade_out=QUEST_MSG_FADEOUT,
            hold_time=QUEST_MSG_HOLDTIME,
            fx_time=QUEST_MSG_FXTIME,
            channel=QUEST_MSG_CHANNEL,
        )
        hud_msg.send(self.player.index)


class RPGPlayerManager(dict):
    def __setitem__(self, key, value):
        dict.__setitem__(self, key, value)
        InternalEvent.fire('rpg_player_registered', rpg_player=value)

    def __delitem__(self, key):
        InternalEvent.fire('rpg_player_unregistered', rpg_player=self[key])
        dict.__delitem__(self, key)

    def get_by_userid(self, userid):
        return self[index_from_userid(userid)]

rpg_player_manager = RPGPlayerManager()


@InternalEvent('player_registered')
def on_player_registered(event_var):
    player = event_var['player']

    if player.steamid != "BOT":
        return

    if requested_mobs:
        mob_class_id = requested_mobs.pop(0)
    else:
        mob_class_id = 1

    character = BotCharacter(mob_class_id)
    rpg_player = BotRPGPlayer(player, character)
    rpg_player_manager[player.index] = rpg_player


@InternalEvent('character_selected')
def on_character_selected(event_var):
    character, player = event_var['character'], event_var['player']

    rpg_player = RPGPlayer(player, character)
    rpg_player_manager[player.index] = rpg_player

    rpg_player.start()


@InternalEvent('player_unregistered')
def on_player_unregistered(event_var):
    player = event_var['player']

    if player.index not in rpg_player_manager:
        return

    rpg_player = rpg_player_manager[player.index]

    if not player.dead:
        rpg_player.store_current_pos()

    rpg_player.character.save_to_database()

    if not rpg_player.is_mob:
        rpg_player.stop()

    del rpg_player_manager[player.index]


def tell(players, message, **tokens):
    if isinstance(players, BaseRPGPlayer):
        players = (players, )

    player_indexes = [rpg_player.player.index for rpg_player in players]

    tokens.update(COLOR_SCHEME)

    message = message.tokenize(**tokens)
    message = strings_common['chat_base'].tokenize(
        message=message, **COLOR_SCHEME)

    SayText2(message=message).send(*player_indexes)


def broadcast(message, **tokens):
    tell(list(rpg_player_manager.values()), message, **tokens)
