from json import dumps, loads

from ..internal_events import InternalEvent

from ..models.character import Character as DB_Character

from ..resource.config_cvars import config_manager

from ..resource.sqlalchemy import Session


class BotError(Exception):
    pass


class BaseCharacter:
    def __init__(self, id_):
        from ..modules.stats import stat_manager

        self.id = id_
        self.name = None

        self._level = 1
        self._skill_data = {}
        self._stat_data = stat_manager.get_initial_stat_data()
        self._quest_data = {
            'current': [],
            'completed': [],
        }
        self.last_pos = None

    @property
    def is_mob(self):
        raise NotImplementedError

    def load_from_database(self):
        raise NotImplementedError

    def save_to_database(self):
        raise NotImplementedError

    @property
    def level(self):
        return self._level

    @property
    def equipment(self):
        raise NotImplementedError

    @property
    def main_inventory(self):
        raise NotImplementedError

    @property
    def bank_inventory(self):
        raise NotImplementedError

    def get_stat(self, id_):
        return self._stat_data[id_].copy()

    def set_stat(self, id_, data):
        self._stat_data[id_] = data


class Character(BaseCharacter):
    def __init__(self, id_):
        super().__init__(id_)

        self.steamid = None

        self.main_inventory_id = -1
        self.bank_inventory_id = -1
        self.equipment_id = -1

        self.spare_stats = 0
        self.exp = 0
        self._spirit = 0

        self._levelup_exp = (
            config_manager['levelup_base_exp'] +
            config_manager['levelup_exp_increase'] * (self.level - 1)
        )

    @property
    def is_mob(self):
        return False

    def load_from_database(self):
        db_session = Session()

        db_character = db_session.query(DB_Character).filter_by(
            id=self.id).first()

        if db_character is not None:
            self.steamid = db_character.steamid
            self.name = db_character.name

            self.main_inventory_id = db_character.main_inventory_id
            self.bank_inventory_id = db_character.bank_inventory_id
            self.equipment_id = db_character.equipment_id

            self._level = db_character.level
            self.exp = db_character.exp
            self._spirit = db_character.spirit
            self.spare_stats = db_character.spare_stats
            self._skill_data.update(loads(db_character.skill_data))
            self._stat_data.update(loads(db_character.stat_data))
            self._quest_data.update(loads(db_character.quest_data))
            self.last_pos = loads(db_character.last_pos)

            self._levelup_exp = (
                config_manager['levelup_base_exp'] +
                config_manager['levelup_exp_increase'] * (self._level - 1)
            )

        db_session.close()

        InternalEvent.fire('db_character_loaded', character=self)

    def save_to_database(self):
        from ..modules.inventory.inventory import player_inventory_manager
        from ..modules.equipment.player_equipment import (
            player_equipment_manager)

        if self.main_inventory_id == -1:
            player_inventory = player_inventory_manager.create_new_inventory(
                self.id)

            self.main_inventory_id = player_inventory.id

        if self.equipment_id == -1:
            player_equipment = (
                player_equipment_manager.create_new_player_equipment(self.id))

            self.equipment_id = player_equipment.id

        db_session = Session()

        db_character = db_session.query(DB_Character).filter_by(
            id=self.id).first()

        if db_character is None:
            db_character = DB_Character()

            db_session.add(db_character)

        db_character.steamid = self.steamid
        db_character.name = self.name

        db_character.main_inventory_id = self.main_inventory_id
        db_character.bank_inventory_id = self.bank_inventory_id
        db_character.equipment_id = self.equipment_id

        db_character.level = self._level
        db_character.exp = self.exp
        db_character.spirit = self._spirit
        db_character.spare_stats = self.spare_stats
        db_character.skill_data = dumps(self._skill_data)
        db_character.stat_data = dumps(self._stat_data)
        db_character.quest_data = dumps(self._quest_data)
        db_character.last_pos = dumps(self.last_pos)

        db_session.commit()

        self.id = db_character.id

        db_session.close()

        InternalEvent.fire('db_character_saved', character=self)

    @property
    def equipment(self):
        from ..modules.equipment.player_equipment import (
            player_equipment_manager)

        return player_equipment_manager[self.equipment_id]

    @property
    def main_inventory(self):
        from ..modules.inventory.inventory import player_inventory_manager

        return player_inventory_manager[self.main_inventory_id]

    @property
    def bank_inventory(self):
        return None

    @property
    def levelup_exp(self):
        return self._levelup_exp

    def check_levelup(self):
        base_exp = config_manager['levelup_base_exp']
        exp_increase = config_manager['levelup_exp_increase']

        new_levels = 0
        while self.exp >= self._levelup_exp:
            new_levels += 1

            self.exp -= self._levelup_exp
            self._level += 1
            self.spare_stats += config_manager['levelup_stats_bonus']

            self._levelup_exp = base_exp + exp_increase * (self._level - 1)

        return new_levels > 0

    def is_quest_started(self, quest):
        return quest.id in self._quest_data['current']

    def is_quest_completed(self, quest):
        return quest.id in self._quest_data['completed']

    def complete_quest(self, quest):
        if not self.is_quest_started(quest):
            raise ValueError("Quest '{}' is not started for character "
                             "(id={})".format(quest.id, self.id))

        self._quest_data['current'].remove(quest.id)

        if quest.id not in self._quest_data['completed']:
            self._quest_data['completed'].append(quest.id)

    def start_quest(self, quest):
        if self.is_quest_started(quest):
            raise ValueError("Quest '{}' is already started for character "
                             "(id={})".format(quest.id, self.id))

        self._quest_data['current'].append(quest.id)

    def is_quest_available(self, quest):
        if self.is_quest_started(quest):
            return False

        if self.is_quest_completed(quest) and not quest.repeatable:
            return False

        return True


class BotCharacter(BaseCharacter):
    def __init__(self, id_):
        super().__init__(id_)

        from ..modules.mobs import mob_class_manager

        mob_class = mob_class_manager[id_]

        self.id = mob_class.id
        self.name = mob_class.name

        self._level = mob_class.level
        self._stat_data = mob_class.stat_data
        self._equipment = mob_class.equipment

    @property
    def is_mob(self):
        return True

    def save_to_database(self):
        pass

    def load_from_database(self):
        pass

    @property
    def equipment(self):
        return self._equipment

    @property
    def main_inventory(self):
        raise BotError("Mobs don't have inventories")

    @property
    def bank_inventory(self):
        raise BotError("Mobs don't have inventories")


class CharacterInfo:
    def __init__(self, db_character):
        self.id = db_character.id
        self.name = db_character.name
        self.level = db_character.level


def get_characters_info_by_steamid(steamid):
    db_session = Session()
    db_characters = db_session.query(
        DB_Character).filter_by(steamid=steamid).all()

    result = [CharacterInfo(db_character) for db_character in db_characters]

    db_session.close()
    return result


def is_name_available(name):
    db_session = Session()
    db_character = db_session.query(DB_Character).filter_by(name=name).first()
    result = db_character is None

    db_session.close()
    return result


def create_new_character(self, steamid, name):
    character = Character(None)

    character.steamid = steamid
    character.name = name

    character.save_to_database()
    return character


def load_character_by_id(self, id_):
    character = Character(id_)

    character.load_from_database()
    return character
