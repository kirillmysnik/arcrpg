from sqlalchemy import Column, Integer, String, Text

from ..resource.sqlalchemy import Base


class Inventory(Base):
    __tablename__ = "inventories"

    id = Column(Integer, primary_key=True)
    owner_type = Column(Integer)
    owner_id = Column(Integer)

    max_slots = Column(Integer)

    slot_data = Column(Text)
