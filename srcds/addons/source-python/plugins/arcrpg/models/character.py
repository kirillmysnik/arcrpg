from sqlalchemy import Column, Integer, String, Text

from ..resource.sqlalchemy import Base


class Character(Base):
    __tablename__ = "characters"

    id = Column(Integer, primary_key=True)
    steamid = Column(String(32))
    name = Column(String(32))

    level = Column(Integer)
    exp = Column(Integer)
    spirit = Column(Integer)
    spare_stats = Column(Integer)
    skill_data = Column(Text)
    stat_data = Column(Text)
    main_inventory_id = Column(Integer)
    bank_inventory_id = Column(Integer)
    equipment_id = Column(Integer)
    quest_data = Column(Text)
    last_pos = Column(Text)

    def __repr__(self):
        return "<Character({})>".format(self.id)
