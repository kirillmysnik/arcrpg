from sqlalchemy import Column, Integer, String, Text

from ..resource.sqlalchemy import Base


class PlayerEquipment(Base):
    __tablename__ = "player_equipments"

    id = Column(Integer, primary_key=True)

    owner_type = Column(Integer)
    owner_id = Column(Integer)

    data = Column(Text)

    def __repr__(self):
        return "<PlayerEquipment({})>".format(self.id)
