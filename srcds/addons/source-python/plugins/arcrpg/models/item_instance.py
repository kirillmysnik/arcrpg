from sqlalchemy import Column, Integer, String, Text

from ..resource.sqlalchemy import Base


class ItemInstance(Base):
    __tablename__ = "item_instances"

    id = Column(Integer, primary_key=True)

    secondary_class_manager_id = Column(String(16))
    secondary_class_id = Column(Integer)

    owner_type = Column(Integer)
    owner_id = Column(Integer)

    amount = Column(Integer)

    stored_data = Column(Text)

    def __repr__(self):
        return "<ItemInstance({})>".format(self.id)
