from collections import defaultdict
from json import dumps, loads

from ...internal_events import InternalEvent

from ...resource.sqlalchemy import Session

from ...models.player_equipment import PlayerEquipment as DB_PlayerEquipment

from ..inventory.item_instance import item_instance_manager
from ..inventory.owner_types import OwnerTypes


class WrongOwnerTypeError(Exception):
    pass


class BaseEquipment(dict):
    def __init__(self):
        super().__init__({
            'primary': -1,
            'secondary': -1,
            'knife': -1,
            'hegrenade': -1,
            'flashbang': -1,
            'smokegrenade': -1,
        })

        self.owner_type = OwnerTypes.NONE
        self.owner_id = 0

    def load_from_database(self):
        raise NotImplementedError

    def save_to_database(self):
        raise NotImplementedError

    def __getitem__(self, key):
        item_instance_id = dict.__getitem__(self, key)
        if item_instance_id == -1:
            return None

        return item_instance_manager[item_instance_id]

    def __setitem__(self, key, item_instance):
        if item_instance is None:
            item_instance_id = -1
        else:
            item_instance_id = item_instance.id

        dict.__setitem__(self, key, item_instance_id)

    def equip(self, item_instance):
        self[item_instance.secondary_class.equip_region] = item_instance

    @property
    def all_equipped_item_instances(self):
        for item_instance_id in self.values():
            if item_instance_id != -1:
                yield item_instance_manager[item_instance_id]


class BotEquipment(BaseEquipment):
    def __init__(self, json):
        super().__init__()

        dict.update(self, json)

    def load_from_database(self):
        pass

    def save_to_database(self):
        pass


class PlayerEquipment(BaseEquipment):
    def __init__(self, id_):
        super().__init__()

        self.id = id_

    def load_from_database(self):
        db_session = Session()

        db_player_equipment = db_session.query(DB_PlayerEquipment).filter_by(
            id=self.id).first()

        if db_player_equipment is not None:
            if db_player_equipment.owner_type != OwnerTypes.PLAYER:
                raise WrongOwnerTypeError("Owner is not a player")

            self.owner_type = db_player_equipment.owner_type
            self.owner_id = db_player_equipment.owner_id

            dict.update(self, loads(db_player_equipment.data))

        db_session.close()

    def save_to_database(self):
        db_session = Session()

        db_player_equipment = db_session.query(DB_PlayerEquipment).filter_by(
            id=self.id).first()

        if db_player_equipment is None:
            db_player_equipment = DB_PlayerEquipment()
            db_session.add(db_player_equipment)

        db_player_equipment.owner_type = OwnerTypes.PLAYER
        db_player_equipment.owner_id = self.owner_id

        db_player_equipment.data = dumps(dict(self))

        db_session.commit()

        self.id = db_player_equipment.id

        db_session.close()


class PlayerEquipmentManager(defaultdict):
    def __missing__(self, key):
        player_equipment = PlayerEquipment(key)
        player_equipment.load_from_database()

        if player_equipment.id is None:
            raise KeyError("Couldn't pick player equipment from "
                           "database (id={})".format(key))

        self[key] = player_equipment

        return player_equipment

    def create_new_player_equipment(self, owner_id):
        player_equipment = PlayerEquipment(None)

        player_equipment.owner_type = OwnerTypes.PLAYER
        player_equipment.owner_id = owner_id

        player_equipment.save_to_database()

        self[player_equipment.id] = player_equipment

        return player_equipment

player_equipment_manager = PlayerEquipmentManager()


@InternalEvent('rpg_player_unregistered')
def on_rpg_player_unregistered(event_var):
    rpg_player = event_var['rpg_player']
    if rpg_player.is_mob:
        return

    if rpg_player.character.equipment_id not in player_equipment_manager:
        return

    player_equipment = player_equipment_manager.pop(
        rpg_player.character.equipment_id)

    player_equipment.save_to_database()
