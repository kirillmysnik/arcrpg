from engines.server import global_vars

from . import register_tick_listener, StatHandler


next_attacks = {}


class AttackRateAlt(StatHandler):
    def register_entity(self, item_instance, entity):
        super().register_entity(item_instance, entity)

        next_attacks[entity.index] = 0

    def unregister_entity(self, item_instance, entity):
        super().unregister_entity(item_instance, entity)

        del next_attacks[entity.index]


stat_handler = AttackRateAlt('attack_rate_alt')


@register_tick_listener
def listener_on_tick():
    for item_instance, entity in stat_handler:
        if entity.next_secondary_fire_attack != next_attacks[entity.index]:
            attack_delay = (
                1.0 / item_instance['stats']['attack_rate_alt'].value1)

            entity.next_secondary_fire_attack = (
                global_vars.current_time + attack_delay)

            next_attacks[entity.index] = entity.next_secondary_fire_attack
