from entities import TakeDamageInfo
from entities.hooks import EntityCondition
from entities.hooks import EntityPreHook
from memory import make_object

from . import StatHandler


class Damage(StatHandler):
    pass

stat_handler = Damage('damage')


@EntityPreHook(EntityCondition.is_player, 'on_take_damage')
def on_take_damage(args):
    info = make_object(TakeDamageInfo, args[1])

    try:
        weapon_index = info.weapon
    except ValueError:
        return

    for item_instance, entity in stat_handler:
        if entity.index == weapon_index:
            break
    else:
        return

    info.damage *= item_instance['stats']['damage'].value1
