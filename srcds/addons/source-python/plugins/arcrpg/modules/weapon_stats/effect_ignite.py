from entities import TakeDamageInfo
from entities.hooks import EntityCondition
from entities.hooks import EntityPreHook
from memory import make_object
from players.entity import Player

from . import StatHandler


class EffectIgnite(StatHandler):
    pass

stat_handler = EffectIgnite('effect_ignite')


@EntityPreHook(EntityCondition.is_player, 'on_take_damage')
def on_take_damage(args):
    info = make_object(TakeDamageInfo, args[1])

    try:
        weapon_index = info.weapon
    except ValueError:
        return

    for item_instance, entity in stat_handler:
        if entity.index == weapon_index:
            break
    else:
        return

    victim = make_object(Player, args[0])
    victim.ignite_lifetime(
        item_instance['stats']['effect_ignite'].value1)
