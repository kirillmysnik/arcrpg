from entities.entity import Entity
from events import Event
from listeners import OnTick
from listeners.tick import Delay

from mathlib import NULL_VECTOR, Vector

from ...classes.rpgplayer import rpg_player_manager

from ...internal_events import InternalEvent

from ...modules.inventory.item_instance import item_instance_manager


stat_handlers = {}


class StatHandler(list):
    def __init__(self, stat_id):
        super().__init__()

        if stat_id not in stat_handlers:
            stat_handlers[stat_id] = []

        stat_handlers[stat_id].append(self)

    def register_entity(self, item_instance, entity):
        self.append((item_instance, entity))

    def unregister_entity(self, item_instance, entity):
        self.remove((item_instance, entity))


tick_listeners = []


class WeaponDispenser:
    def __init__(self):
        self._instances_to_entities = {}

    def connect(self, item_instance, player_index, entity):
        # First things first, make sure that previous entity is disconnected
        if item_instance.id in self._instances_to_entities:
            if player_index in self._instances_to_entities[item_instance.id]:
                self.disconnect(item_instance, player_index)

        # Only then connect a new entity
        if item_instance.id not in self._instances_to_entities:
            self._instances_to_entities[item_instance.id] = {}

        self._instances_to_entities[item_instance.id][player_index] = entity

        for stat_id, stat in item_instance['stats'].items():
            for stat_handler in stat_handlers.get(stat_id, ()):
                stat_handler.register_entity(item_instance, entity)

        return entity

    def disconnect(self, item_instance, player_index):
        entity = self._instances_to_entities[item_instance.id].pop(
            player_index)

        if not self._instances_to_entities[item_instance.id]:
            del self._instances_to_entities[item_instance.id]

        for stat_id, stat in item_instance['stats'].items():
            for stat_handler in stat_handlers.get(stat_id, ()):
                stat_handler.unregister_entity(item_instance, entity)

        return entity

    def create_entity_and_connect(self, player, item_instance):
        entity = Entity.create(item_instance.secondary_class.entity_classname)
        self.connect(item_instance, player.index, entity)

        entity.spawn()
        entity.teleport(player.eye_location, None, None)

    def remove_entity_and_disconnect(self, player, item_instance):
        entity = self.disconnect(item_instance, player.index)

        if entity.index in player.weapon_indexes():
            player.drop_weapon(entity.pointer, NULL_VECTOR, NULL_VECTOR)

        entity.remove()

    def on_tick(self):
        entities_to_disconnect = []
        for item_instance_id, entities in self._instances_to_entities.items():
            item_instance = item_instance_manager[item_instance_id]
            for player_index, entity in entities.items():
                # Some dirty hacks incoming
                try:
                    entity.classname
                except RuntimeError:
                    entities_to_disconnect.append((
                        item_instance, player_index))

        for item_instance, player_index in entities_to_disconnect:
            self.disconnect(item_instance, player_index)

        for callback in tick_listeners:
            callback()

weapon_dispenser = WeaponDispenser()


@InternalEvent('player_respawn')
def on_player_respawn(event_var):
    player = event_var['player']
    rpg_player = rpg_player_manager[player.index]
    Delay(0, rpg_player.reequip)


@Event('player_death')
def on_player_death(game_event):
    rpg_player = rpg_player_manager.get_by_userid(game_event['userid'])

    for item_instance in (
         rpg_player.character.equipment.all_equipped_item_instances):

        weapon_dispenser.remove_entity_and_disconnect(
            rpg_player.player, item_instance)


@OnTick
def listener_on_tick():
    weapon_dispenser.on_tick()


def register_tick_listener(callback):
    if callback in tick_listeners:
        raise ValueError("{} is already a registered tick listener".format(
            callback))

    tick_listeners.append(callback)


def unregister_tick_listener(callback):
    tick_listeners.remove(callback)


import os

from .. import parse_modules

__all__ = parse_modules(os.path.dirname(__file__))

from . import *
