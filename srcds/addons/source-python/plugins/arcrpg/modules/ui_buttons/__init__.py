from entities.hooks import EntityCondition, EntityPreHook
from memory import make_object
from players import UserCmd
from players.entity import Player

from ...internal_events import InternalEvent


TRIGGER_INTERVAL = 5

intervals = {}
pressed_buttons = {}
button_callbacks = {}


def register_button_callback(button, callback):
    if button not in button_callbacks:
        button_callbacks[button] = []

    if callback in button_callbacks[button]:
        raise ValueError("Button callback '{}' is already "
                         "registered".format(callback))

    button_callbacks[button].append(callback)


def unregister_button_callback(button, callback):
    button_callbacks[button].remove(callback)

    if not button_callbacks[button]:
        del button_callbacks[button]


@EntityPreHook(EntityCondition.is_human_player, 'run_command')
def pre_run_command(args):
    player = make_object(Player, args[0])

    intervals[player.index] += 1
    if intervals[player.index] >= TRIGGER_INTERVAL:
        intervals[player.index] = 0
    else:
        return

    user_cmd = make_object(UserCmd, args[1])

    for button, callbacks in button_callbacks.items():
        if user_cmd.buttons & button == button:
            if not pressed_buttons[player.index] & button == button:
                for callback in callbacks:
                    callback(player)

                pressed_buttons[player.index] |= button

        else:
            pressed_buttons[player.index] &= ~button


@InternalEvent('player_registered')
def on_player_registered(event_var):
    player = event_var['player']

    pressed_buttons[player.index] = 0
    intervals[player.index] = 0


@InternalEvent('player_unregistered')
def on_player_unregistered(event_var):
    player = event_var['player']

    del pressed_buttons[player.index]
    del intervals[player.index]
