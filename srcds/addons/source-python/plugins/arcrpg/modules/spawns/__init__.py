from events import Event

from ...classes.rpgplayer import rpg_player_manager

from ...internal_events import InternalEvent

from . import last_pos_updater

from .mapdata import reload_spawnpoints, spawnpoints


@Event('server_spawn')
def on_server_spawn(game_event):
    reload_spawnpoints()


@InternalEvent('load')
def on_load(arc_event):
    reload_spawnpoints()


@InternalEvent('player_respawn')
def on_player_respawn(event_var):
    player = event_var['player']
    rpg_player = rpg_player_manager[player.index]

    if rpg_player.character.is_mob:
        # TODO: Handle mob respawning
        pass

    else:
        if rpg_player.character.last_pos is None:
            rpg_player.player.teleport(
                spawnpoints['new_player'].origin,
                spawnpoints['new_player'].angles,
                None
            )

        else:
            rpg_player.restore_last_pos()
