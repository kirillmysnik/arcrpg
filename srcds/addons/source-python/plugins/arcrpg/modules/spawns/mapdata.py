from json import load as json_load
from warnings import warn

from engines.server import global_vars

from ...classes.rpgplayer import vector_from_str

from ...resource.paths import MAPDATA_PATH


class MissingSPFileWarning(Warning):
    pass


class Spawnpoint:
    def __init__(self, json):
        self.origin = vector_from_str(json['origin'])
        self.angles = vector_from_str(json['angles'])


spawnpoints = {}


def reload_spawnpoints():
    spawnpoints.clear()

    if not global_vars.map_name:
        return

    path = MAPDATA_PATH / global_vars.map_name / "spawnpoints.json"
    if not path.isfile():
        warn(MissingSPFileWarning("File '{}' doesn't exist".format(path)))
        return

    with open(path) as f:
        spawnpoints_json = json_load(f)

    spawnpoints['new_player'] = Spawnpoint(spawnpoints_json['new_player'])
