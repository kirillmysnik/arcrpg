from players.helpers import get_client_language

from ...classes.rpgplayer import rpg_player_manager

from ...internal_events import InternalEvent

from ...resource.strings import build_module_strings

from . import Stat


HP_PER_LEVEL = 10

strings_module = build_module_strings('stats.health')


def get_max_health(level):
    return HP_PER_LEVEL * level


class StatHealth(Stat):
    name = strings_module['name']
    description = strings_module['description']
    init_level = 10

    def get_player_class_entry(self, rpg_player):
        level = self.get_player_stat_level(rpg_player)
        return (
            strings_module['class_entry'].get_string(
                get_client_language(rpg_player.player.index)),
            get_max_health(level),
        )

stat_health = StatHealth('health')


@InternalEvent('player_respawn')
def on_player_respawn(event_var):
    player = event_var['player']
    rpg_player = rpg_player_manager[player.index]

    level = stat_health.get_player_stat_level(rpg_player)
    max_health = get_max_health(level)

    player.max_health = max_health
    player.health = max_health


@InternalEvent('stat_level_changed')
def on_stat_level_changed(event_var):
    rpg_player = event_var['rpg_player']

    if event_var['stat_id'] == 'health':
        max_health = get_max_health(event_var['new_level'])
        rpg_player.player.max_health = max_health
        rpg_player.player.health = max_health
