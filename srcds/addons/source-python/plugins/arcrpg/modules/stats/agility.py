from events import Event
from players.helpers import get_client_language

from ...classes.rpgplayer import rpg_player_manager

from ...internal_events import InternalEvent

from ...resource.strings import build_module_strings

from . import build_stats_config, Stat


config_manager = build_stats_config('agility')
cvar_max_level = config_manager.cvar(
    name="max_level",
    default=205,
    description="",
)
cvar_add_per_level = config_manager.cvar(
    name="add_per_level",
    default=0.01,
    description="",
)
cvar_initial_multiplier = config_manager.cvar(
    name="initial_multiplier",
    default=0.95,
    description="",
)

strings_module = build_module_strings('stats.agility')


def get_speed(level):
    return (cvar_initial_multiplier.get_float() +
            cvar_add_per_level.get_float() * level)


class StatAgility(Stat):
    name = strings_module['name']
    description = strings_module['description']
    init_level = 5
    max_level = cvar_max_level

    def get_player_class_entry(self, rpg_player):
        level = self.get_player_stat_level(rpg_player)
        return (
            strings_module['class_entry_name'].get_string(
                get_client_language(rpg_player.player.index)),
            strings_module['class_entry_value'].tokenize(
                value=get_speed(level),
            ).get_string(get_client_language(rpg_player.player.index))
        )

stat_agility = StatAgility('agility')


@InternalEvent('player_respawn')
def on_player_respawn(event_var):
    player = event_var['player']
    rpg_player = rpg_player_manager[player.index]

    level = stat_agility.get_player_stat_level(rpg_player)
    speed = get_speed(level)

    player.speed = speed


@Event('player_jump')
def on_player_jump(game_event):
    rpg_player = rpg_player_manager.get_by_userid(game_event['userid'])
    level = stat_agility.get_player_stat_level(rpg_player)
    gravity = 1 / get_speed(level)**2
    rpg_player.player.gravity = gravity


@InternalEvent('stat_level_changed')
def on_stat_level_changed(event_var):
    rpg_player = event_var['rpg_player']

    if event_var['stat_id'] == 'agility':
        speed = get_speed(event_var['new_level'])
        rpg_player.player.speed = speed
