from entities import TakeDamageInfo
from entities.constants import WORLD_ENTITY_INDEX
from entities.hooks import EntityCondition
from entities.hooks import EntityPreHook
from memory import make_object
from players.entity import Player
from players.helpers import get_client_language

from ...classes.rpgplayer import rpg_player_manager
from ...resource.strings import build_module_strings

from . import build_stats_config, Stat


strings_module = build_module_strings('stats.damage')

config_manager = build_stats_config('damage')
cvar_add_per_level = config_manager.cvar(
    name="add_per_level",
    default=0.05,
    description="",
)
cvar_initial_multiplier = config_manager.cvar(
    name="initial_multiplier",
    default=0.75,
    description="",
)


def get_damage(level):
    return (cvar_initial_multiplier.get_float() +
            cvar_add_per_level.get_float() * level)


class StatDamage(Stat):
    name = strings_module['name']
    description = strings_module['description']
    init_level = 5

    def get_player_class_entry(self, rpg_player):
        level = self.get_player_stat_level(rpg_player)
        return (
            strings_module['class_entry_name'].get_string(
                get_client_language(rpg_player.player.index)),
            strings_module['class_entry_value'].tokenize(
                value=get_damage(level),
            ).get_string(get_client_language(rpg_player.player.index))
        )

stat_damage = StatDamage('damage')


@EntityPreHook(EntityCondition.is_player, 'on_take_damage')
def on_take_damage(args):
    victim = make_object(Player, args[0])
    info = make_object(TakeDamageInfo, args[1])

    if info.attacker == victim.index:
        return

    # Check for non-player entities like trigger_hurt's, worldspawn etc
    if info.attacker not in rpg_player_manager:
        return

    rpg_player = rpg_player_manager[info.attacker]
    level = stat_damage.get_player_stat_level(rpg_player)

    info.damage *= get_damage(level)
