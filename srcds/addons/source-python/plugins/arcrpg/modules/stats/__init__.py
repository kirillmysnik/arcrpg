from config.manager import ConfigManager
from players.helpers import get_client_language
from translations.strings import TranslationStrings

from ...info import info


class Stat:
    name = None
    description = None
    max_level = None
    init_level = 5

    def __init__(self, id_):
        self.id = id_
        stat_manager[id_] = self

    def get_player_stat_level(self, rpg_player):
        stat_data = rpg_player.character.get_stat(self.id)
        return stat_data['level']

    def set_player_stat_level(self, rpg_player, level):
        stat_data = rpg_player.character.get_stat(self.id)
        stat_data['level'] = level
        rpg_player.character.set_stat(self.id, stat_data)

    def get_player_class_entry(self, rpg_player):
        raise NotImplementedError

    def get_name(self, rpg_player):
        if isinstance(self.name, TranslationStrings):
            return self.name.get_string(
                get_client_language(rpg_player.player.index))

        return self.name

    def get_description(self, rpg_player):
        if isinstance(self.description, TranslationStrings):
            return self.description.get_string(
                get_client_language(rpg_player.player.index))

        return self.description


class StatManager(dict):
    def get_initial_stat_data(self):
        result = {}
        for id_, stat in self.items():
            result[id_] = {'level': stat.init_level}
        return result

stat_manager = StatManager()


configs = []


def build_stats_config(path):
    config_file = info.basename + '/' + info.basename + '_stat_{}'.format(path)
    config_manager = ConfigManager(
        config_file, cvar_prefix='arcrpg_stat_{}'.format(path))

    configs.append(config_manager)
    return config_manager


import os

from .. import parse_modules


__all__ = parse_modules(os.path.dirname(__file__))

from . import *

for config_manager in configs:
    config_manager.write()
    config_manager.execute()
