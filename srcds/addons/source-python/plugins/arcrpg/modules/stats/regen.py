from events import Event
from listeners.tick import Delay
from players.helpers import get_client_language

from ...classes.rpgplayer import rpg_player_manager

from ...internal_events import InternalEvent

from ...resource.strings import build_module_strings

from . import Stat


BASE_HP_AMOUNT = 0
HP_AMOUNT_INCREASE = 3
REGEN_INTERVAL = 2

strings_module = build_module_strings('stats.regen')


def get_regen_amount(level):
    return BASE_HP_AMOUNT + HP_AMOUNT_INCREASE * level


def do_regen(rpg_player):
    if (player_delays[rpg_player.player.index] is not None and
            player_delays[rpg_player.player.index].running):

        player_delays[rpg_player.player.index].cancel()

    rpg_player.player.health = min(
        rpg_player.player.max_health,
        rpg_player.player.health + get_regen_amount(
            stat_regen.get_player_stat_level(rpg_player)
        ),
    )

    player_delays[rpg_player.player.index] = Delay(
        REGEN_INTERVAL, do_regen, rpg_player)


class StatRegen(Stat):
    name = strings_module['name']
    description = strings_module['description']
    init_level = 0

    def get_player_class_entry(self, rpg_player):
        level = self.get_player_stat_level(rpg_player)
        return (
            strings_module['class_entry_name'].get_string(
                get_client_language(rpg_player.player.index)),
            strings_module['class_entry_value'].tokenize(
                value=get_regen_amount(level) / REGEN_INTERVAL,
            ).get_string(get_client_language(rpg_player.player.index))
        )

stat_regen = StatRegen('regen')


player_delays = {}


@InternalEvent('rpg_player_registered')
def on_rpg_player_registered(event_var):
    player_delays[event_var['rpg_player'].player.index] = None


@InternalEvent('rpg_player_unregistered')
def on_rpg_player_unregistered(event_var):
    rpg_player = event_var['rpg_player']
    if (player_delays[rpg_player.player.index] is not None and
            player_delays[rpg_player.player.index].running):

        player_delays[rpg_player.player.index].cancel()

    del player_delays[rpg_player.player.index]


@InternalEvent('player_respawn')
def on_player_respawn(event_var):
    player = event_var['player']
    rpg_player = rpg_player_manager[player.index]

    player_delays[player.index] = Delay(REGEN_INTERVAL, do_regen, rpg_player)


@Event('player_death')
def on_player_death(game_event):
    rpg_player = rpg_player_manager.get_by_userid(game_event['userid'])
    if (player_delays[rpg_player.player.index] is not None and
            player_delays[rpg_player.player.index].running):

        player_delays[rpg_player.player.index].cancel()
