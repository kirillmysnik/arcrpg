from ..motdplayer import plugin_instance


def send_charselect_screen(player):
    def json_charselect_callback(data, error):
        if error is not None:
            return

        return {
            'characters': [],
        }

    def json_charselect_retargeting_callback(new_page_id):
        return None

    def charselect_callback(data, error):
        pass

    def charselect_retargeting_callback(new_page_id):
        if new_page_id == "json-charselect":
            return (json_charselect_callback,
                    json_charselect_retargeting_callback)

    plugin_instance.send_page(
        player, 'charselect', charselect_callback,
        charselect_retargeting_callback, debug=True)
