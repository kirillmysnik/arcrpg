from commands.client import ClientCommand

from ...arcrpg import player_manager

from ...classes.rpgplayer import rpg_player_manager

from ...internal_events import InternalEvent

from .motd import send_charselect_screen


PLAYER_TEAM = 3


@ClientCommand("jointeam")
def client_jointeam(command, index):
    if index in rpg_player_manager:
        return False

    player = player_manager[index]
    send_charselect_screen(player)

    return False


@InternalEvent('rpg_player_registered')
def on_rpg_player_registered(event_var):
    rpg_player = event_var['rpg_player']

    if rpg_player.is_mob:
        return

    rpg_player.player.team = PLAYER_TEAM
