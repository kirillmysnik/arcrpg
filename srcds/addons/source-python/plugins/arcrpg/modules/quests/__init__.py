from json import load

from ...internal_events import InternalEvent

from ...resource.paths import ARCRPG_DATA_PATH

from ...resource.strings import strings_quests


QUEST_DATA_DIR = ARCRPG_DATA_PATH / "quests"


class Quest:
    def __init__(self, id_, quest_json):
        self.id = id_
        self.title = strings_quests[quest_json['title']]
        self.description = strings_quests[quest_json['desc']]
        self.exp_reward = quest_json['exp_reward']
        self.cash_reward = quest_json['cash_reward']
        self.can_be_started_after_failure = quest_json[
            'can_be_started_after_failure']

        self.completed_quests_available_after = quest_json[
            'completed_quests_available_after']

        self.completed_quests_available_before = quest_json[
            'completed_quests_available_before']

        self.started_quests_available_after = quest_json[
            'started_quests_available_after']

        self.started_quests_available_before = quest_json[
            'started_quests_available_before']

        self.level_restriction_min = quest_json['level_restriction_min']
        self.level_restriction_max = quest_json['level_restriction_max']

    def start(self, rpg_player):
        InternalEvent.fire('quest_started', quest=self, rpg_player=rpg_player)

    def complete(self, rpg_player):
        InternalEvent.fire(
            'quest_completed', quest=self, rpg_player=rpg_player)

    def available(self, rpg_player):
        if rpg_player.is_quest_started(self):
            return False

        if (self.level_restriction_min > 0 and
                rpg_player.character.level < self.level_restriction_min):

            return False

        if (self.level_restriction_max > 0 and
                rpg_player.character.level > self.level_restriction_max):

            return False

        for quest_id in self.completed_quests_available_after:
            if not rpg_player.is_quest_completed(quests[quest_id]):
                return False

        for quest_id in self.completed_quests_available_before:
            if rpg_player.is_quest_completed(quests[quest_id]):
                return False

        for quest_id in self.started_quests_available_after:
            if not rpg_player.is_quest_started(quests[quest_id]):
                return False

        for quest_id in self.started_quests_available_before:
            if rpg_player.is_quest_started(quests[quest_id]):
                return False

        return True


quests = {}

for json_file in QUEST_DATA_DIR.files():
    with open(json_file, 'r') as f:
        id_ = json_file.namebase
        quests[id_] = Quest(id_, load(f))


import os

from .. import parse_modules


__all__ = parse_modules(os.path.dirname(__file__))

from . import *
