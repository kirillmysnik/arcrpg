from json import load as json_load
from warnings import warn

from engines.server import global_vars
from mathlib import Vector

from ...classes.rpgplayer import vector_from_str

from ...resource.paths import MAPDATA_PATH

from .classes import npc_class_manager


class MissingNPCFileWarning(Warning):
    pass


class NPCMapDataEntry:
    def __init__(self, json):
        self.npc_class = npc_class_manager[json['class']]
        self.origin = vector_from_str(json['origin'])
        self.angles = vector_from_str(json['angles'])


npc_list = []


def reload_npc_list():
    npc_list.clear()

    if not global_vars.map_name:
        return

    path = MAPDATA_PATH / global_vars.map_name / "npc.json"
    if not path.isfile():
        warn(MissingNPCFileWarning("File '{}' doesn't exist".format(path)))
        return

    with open(path) as f:
        npcs_json = json_load(f)['npcs']

    for npc_json in npcs_json:
        npc_list.append(NPCMapDataEntry(npc_json))
