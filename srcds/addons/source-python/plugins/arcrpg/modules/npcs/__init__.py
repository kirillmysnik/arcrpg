from events import Event

from ...internal_events import InternalEvent

from . import player_use

from .instances import NPCInstance

from .mapdata import npc_list, reload_npc_list


existing_npcs = []


@Event('server_spawn')
def on_server_spawn(game_event):
    reload_npc_list()


@InternalEvent('load')
def on_load(arc_event):
    reload_npc_list()


@Event('round_start')
def on_round_start(game_event):
    existing_npcs.clear()

    for npc_map_data_entry in npc_list:
        npc = NPCInstance(
            npc_map_data_entry.npc_class, npc_map_data_entry.origin,
            npc_map_data_entry.angles)

        npc.spawn()

        existing_npcs.append(npc)
