from json import load

from engines.precache import Model
from mathlib import Vector

from ...resource.paths import ARCRPG_DATA_PATH

from ...resource.strings import strings_quests


NPC_CLASSES_DIR = ARCRPG_DATA_PATH / "npc_classes"


class NPCClass:
    def __init__(self, id_, json):
        self.id = id_
        self.name = strings_quests[json['name']]
        self.talk = strings_quests[json['talk']]
        self.model = Model(json['model'])
        self.animation = json['animation']
        self.sprite = Model(json['sprite'])
        self.sprite_offset = Vector(0, 0, json['sprite_offset_z'])


npc_class_manager = {}

for json_file in NPC_CLASSES_DIR.files():
    with open(json_file, 'r') as f:
        id_ = json_file.namebase
        npc_class_manager[id_] = NPCClass(id_, load(f))
