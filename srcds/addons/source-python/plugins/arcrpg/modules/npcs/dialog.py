from json import load as json_load
from random import choice

from engines.sound import Sound
from menus import SimpleOption
from menus.base import Text
from menus.radio import SimpleRadioMenu

from ...classes.rpgplayer import rpg_player_manager

from ...resource.paths import ARCRPG_DATA_PATH

from ...resource.strings import build_module_strings, strings_quests

from ..quests import quests


DIALOGS_DIR = ARCRPG_DATA_PATH / "dialogs"
DIALOG_CLICK_SOUND = Sound('ui/buttonrollover.wav')

strings_module = build_module_strings('npcs.dialog')


class DialogRadioMenu(SimpleRadioMenu):
    @staticmethod
    def _slots_to_bin(slots):
        """Convert an iterable of slots to the binary slot representation.

        :param iterable slots: Slots that should be enabled.
        :raise ValueError: Raised if a slot is out of range.
        """
        # Keys are enabled in that order: 0987654321
        buffer = list('0000000000')
        for slot in slots:
            if 0 <= slot <= 9:
                buffer[~(slot - 1)] = '1'
            else:
                raise ValueError('Slot out of range: {}'.format(slot))

        # Disable BUTTON_CLOSE
        buffer[0] = '0'

        return int(''.join(buffer), 2)


class DialogFrameAnswer:
    def __init__(self, answer_json):
        self._random_response_id = answer_json['text']
        self.action = answer_json['action']
        self.param = answer_json.get('param')

    @property
    def text(self):
        if self._random_response_id in random_responses:
            text_id = choice(random_responses[self._random_response_id])
            return strings_quests[text_id]

        return strings_quests[self._random_response_id]


class DialogFrame:
    def __init__(self, frame_json):
        self.id = frame_json['id']
        self.text = strings_quests[frame_json['text']]
        self.answers = []

        for answer_json in frame_json['answers']:
            self.answers.append(DialogFrameAnswer(answer_json))


class Dialog:
    def __init__(self, id_, dialog_json):
        self.id = id_
        self.npc_classes = dialog_json['npcs']
        self.completed_quests_available_after = dialog_json[
            'completed_quests_available_after']

        self.completed_quests_available_before = dialog_json[
            'completed_quests_available_before']

        self.started_quests_available_after = dialog_json[
            'started_quests_available_after']

        self.started_quests_available_before = dialog_json[
            'started_quests_available_before']

        self.quest_availability_bound_to = dialog_json[
            'quest_availability_bound_to']

        self.level_restriction_min = dialog_json['level_restriction_min']
        self.level_restriction_max = dialog_json['level_restriction_max']
        self.initial_answer = DialogFrameAnswer(dialog_json['init'])

        self.frames = {}
        for frame_json in dialog_json['frames']:
            frame = DialogFrame(frame_json)
            self.frames[frame.id] = frame

    def available(self, rpg_player):
        if (self.level_restriction_min > 0 and
                rpg_player.character.level < self.level_restriction_min):

            return False

        if (self.level_restriction_max > 0 and
                rpg_player.character.level > self.level_restriction_max):

            return False

        for quest_id in self.completed_quests_available_after:
            if not rpg_player.is_quest_completed(quests[quest_id]):
                return False

        for quest_id in self.completed_quests_available_before:
            if rpg_player.is_quest_completed(quests[quest_id]):
                return False

        for quest_id in self.started_quests_available_after:
            if not rpg_player.is_quest_started(quests[quest_id]):
                return False

        for quest_id in self.started_quests_available_before:
            if rpg_player.is_quest_started(quests[quest_id]):
                return False

        if (self.quest_availability_bound_to is not None and
                not quests[self.quest_availability_bound_to].available(
                    rpg_player)):

            return False

        return True


class DialogManager(dict):
    def get_npc_class_dialogs(self, npc_class):
        result = []
        for dialog in self.values():
            if npc_class.id in dialog.npc_classes:
                result.append(dialog)

        return result

dialog_manager = DialogManager()

for json_file in DIALOGS_DIR.files():
    with open(json_file, 'r') as f:
        id_ = json_file.namebase
        dialog_manager[id_] = Dialog(id_, json_load(f))

with open(ARCRPG_DATA_PATH / "random_responses.json", 'r') as f:
    random_responses = json_load(f)

_popups = {}
_walk_by_answer = DialogFrameAnswer({
    'text': 'uni walk_by',
    'action': "DIALOG_END",
})


def npc_dialog_screen_callback(popup, index, option):
    rpg_player = rpg_player_manager[index]
    npc, dialog, answer = option.value

    DIALOG_CLICK_SOUND.play(rpg_player.player.index)

    if answer.action == "DIALOG_END":
        rpg_player.player.frozen = False
        return

    if answer.action == "NEXT_SPEECH":
        if rpg_player.player.index in _popups:
            _popups[rpg_player.player.index].close()

        popup = _popups[rpg_player.player.index] = DialogRadioMenu(
            select_callback=npc_dialog_screen_callback)

        popup.append(Text(strings_module['popup header'].tokenize(
            name=npc.npc_class.name)))

        frame = dialog.frames[answer.param]
        popup.append(Text(frame.text))
        popup.append(Text(strings_module['popup answer_breaker']))

        for i, answer in enumerate(frame.answers, start=1):
            popup.append(SimpleOption(i, answer.text, (
                npc, dialog, answer)))

        popup.send(rpg_player.player.index)

        return

    if answer.action == "QUEST_START":
        rpg_player.player.frozen = False

        quest = quests[answer.param]
        rpg_player.start_quest(quest)

        return

    if answer.action == "QUEST_COMPLETE":
        rpg_player.player.frozen = False

        quest = quests[answer.param]
        rpg_player.complete_quest(quest)

        return


def send_npc_dialog_screen(npc, player):
    if player.index in _popups:
        _popups[player.index].close()

    rpg_player = rpg_player_manager[player.index]

    popup = _popups[player.index] = DialogRadioMenu(
        select_callback=npc_dialog_screen_callback)

    popup.append(Text(strings_module['popup header'].tokenize(
        name=npc.npc_class.name)))

    popup.append(Text(npc.npc_class.talk))
    popup.append(Text(strings_module['popup answer_breaker']))

    i = 0
    for dialog in dialog_manager.get_npc_class_dialogs(npc.npc_class):
        if not dialog.available(rpg_player):
            continue

        i += 1
        popup.append(SimpleOption(i, dialog.initial_answer.text, (
            npc, dialog, dialog.initial_answer)))

    popup.append(SimpleOption(
        i + 1, _walk_by_answer.text, (npc, None, _walk_by_answer)))

    popup.send(player.index)

    player.base_velocity = player.velocity * (-1)
    player.frozen = True
