from engines.trace import (
    ContentMasks, GameTrace, engine_trace, Ray, TraceFilterSimple)

from players.constants import PlayerButtons

from ..ui_buttons import register_button_callback

from .dialog import send_npc_dialog_screen


NPC_DIALOG_BUTTON = PlayerButtons.USE
MAX_NPC_DIALOG_DISTANCE = 128


def button_callback(player):
    start_vec = player.eye_location
    end_vec = start_vec + player.view_vector * MAX_NPC_DIALOG_DISTANCE

    trace = GameTrace()

    engine_trace.trace_ray(
        Ray(start_vec, end_vec), ContentMasks.ALL,
        TraceFilterSimple((player.index,)), trace
    )

    if not trace.did_hit():
        return

    from . import existing_npcs
    for npc in existing_npcs:
        if npc.entity is not None and npc.entity.index == trace.entity_index:
            break
    else:
        return

    send_npc_dialog_screen(npc, player)

register_button_callback(NPC_DIALOG_BUTTON, button_callback)
