from entities.constants import RenderMode, SolidType
from entities.entity import Entity


class NPCInstance:
    def __init__(self, npc_class, origin, angles):
        self.npc_class = npc_class
        self.origin = origin
        self.angles = angles

        self._prop = None
        self._sprite = None

    @property
    def entity(self):
        return self._prop

    def spawn(self):
        if self._prop is not None:
            raise RuntimeError("NPCInstance is already spawned")

        self._prop = Entity.create('prop_dynamic_override')
        self._prop.model = self.npc_class.model
        self._prop.default_anim = self.npc_class.animation
        self._prop.solid_type = SolidType.VPHYSICS
        self._prop.teleport(self.origin, self.angles, None)
        self._prop.spawn()

        self._sprite = Entity.create('env_sprite')
        self._sprite.model = self.npc_class.sprite
        self._sprite.set_key_value_float('scale', 0.2)
        self._sprite.render_mode = RenderMode.TRANS_COLOR

        self._sprite.teleport(
            self.origin + self.npc_class.sprite_offset, None, None)

        self._sprite.spawn()

        self._sprite.set_key_value_int('moveparent', self._prop.inthandle)

    def remove(self):
        if self._prop is None:
            raise RuntimeError("NPCInstance is not spawned")

        self._prop.remove()
        self._prop = None

