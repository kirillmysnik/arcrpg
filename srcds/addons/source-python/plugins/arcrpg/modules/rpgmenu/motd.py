from commands.client import ClientCommand
from players.entity import Player
from players.helpers import get_client_language

from ...classes.rpgplayer import rpg_player_manager

from ...internal_events import InternalEvent

from ..inventory.item_instance import item_instance_manager

from ..motdplayer import plugin_instance


def send_menu(player):
    from ..stats import stat_manager

    rpg_player = rpg_player_manager[player.index]

    def json_index_callback(data, error):
        if error is not None:
            return

        if data['action'] == "apply-stats-changes":
            for stat_id, stat_level in data['stats_data'].items():
                stat = stat_manager[stat_id]
                current_level = stat.get_player_stat_level(rpg_player)

                if (stat.max_level is not None and
                        stat.max_level.get_int() < stat_level):

                    continue

                if current_level >= stat_level:
                    continue

                if (stat_level - current_level >
                        rpg_player.character.spare_stats):

                    continue

                rpg_player.character.spare_stats -= stat_level - current_level
                stat.set_player_stat_level(rpg_player, stat_level)

                InternalEvent.fire(
                    'stat_level_changed',
                    rpg_player=rpg_player,
                    stat_id=stat_id,
                    old_level=current_level,
                    new_level=stat_level,
                )

        stats = []
        for stat in stat_manager.values():
            class_entry_name, class_entry_value = stat.get_player_class_entry(
                rpg_player)

            stats.append({
                'id': stat.id,
                'class_entry_name': class_entry_name,
                'class_entry_value': class_entry_value,
                'name': stat.get_name(rpg_player),
                'level': stat.get_player_stat_level(rpg_player),
                'description': stat.get_description(rpg_player),
            })

        return {
            'chardata': {
                'level': rpg_player.character.level,
                'exp': rpg_player.character.exp,
                'levelup_exp': rpg_player.character.levelup_exp,
                'stats': stats,
                'spare_stats': rpg_player.character.spare_stats,
            }
        }

    def json_index_retargeting_callback(new_page_id):
        if new_page_id == "inventory":
            return inventory_callback, inventory_retargeting_callback

        return None

    def index_callback(data, error):
        pass

    def index_retargeting_callback(new_page_id):
        if new_page_id == "json-index":
            return json_index_callback, json_index_retargeting_callback

    def json_inventory_callback(data, error):
        if error is not None:
            return

        inventory = rpg_player.character.main_inventory
        equipment = rpg_player.character.equipment

        if data['action'] == 'equip':
            try:
                item_instance = item_instance_manager[data['item_instance_id']]
            except KeyError:
                pass
            else:
                if item_instance in inventory:
                    equip_region = item_instance.secondary_class.equip_region

                    if equipment[equip_region] == item_instance:
                        equipment[equip_region] = None
                    else:
                        equipment[equip_region] = item_instance

                    rpg_player.reequip()

        language = get_client_language(player.index)

        inventory_json = []
        equipped_ids = [item_instance.id for item_instance in
                        equipment.all_equipped_item_instances]

        def get_item_instance_json(item_instance):
            if item_instance is None:
                return {'empty': True}

            item_instance_json = {
                'id': item_instance.id,

                'icon': item_instance.secondary_class.icon,

                'quality_color': item_instance.secondary_class.quality.color,

                'quality_name': (
                    item_instance.secondary_class.quality.name.get_string(
                        language)),

                'name': (
                    item_instance.secondary_class.name.get_string(language)),

                'description': (
                    item_instance.secondary_class.description.get_string(
                        language)),

                'buy_price': item_instance.secondary_class.price,
            }

            if item_instance.secondary_class.manager_id == 'weapon':
                item_instance_json['equipped'] = (
                    item_instance.id in equipped_ids)

                item_instance_json['stats'] = []

                stats = dict(item_instance.secondary_class.stats)
                stats.update(item_instance['stats'])

                for stat in stats.values():
                    item_instance_json['stats'].append({
                        'text': stat.name.get_string(language,
                                                     value1=stat.value1,
                                                     value2=stat.value2,
                                                     value3=stat.value3,
                                                     value4=stat.value4),
                        'quality_color': stat.quality.color,
                    })

            return item_instance_json

        for item_instance in inventory:
            inventory_json.append(get_item_instance_json(item_instance))

        for i in range(len(inventory_json), inventory.max_slots):
            inventory_json.append({'empty': True})

        return {
            'inventory': {
                'row_length': 10,
                'items': inventory_json,
                'equipment': {
                    'primary': get_item_instance_json(equipment['primary']),

                    'secondary': get_item_instance_json(
                        equipment['secondary']),

                    'knife': get_item_instance_json(equipment['knife']),

                    'hegrenade': get_item_instance_json(
                        equipment['hegrenade']),

                    'flashbang': get_item_instance_json(
                        equipment['flashbang']),

                    'smokegrenade': get_item_instance_json(
                        equipment['smokegrenade']),
                },
            },
        }

    def json_inventory_retargeting_callback(new_page_id):
        if new_page_id == "index":
            return index_callback, index_retargeting_callback

    def inventory_callback(data, error):
        pass

    def inventory_retargeting_callback(new_page_id):
        if new_page_id == "json-inventory":
            return json_inventory_callback, json_inventory_retargeting_callback

    plugin_instance.send_page(player, 'index', index_callback,
                              index_retargeting_callback, debug=False)


@ClientCommand('rpgmenu')
def client_rpgmenu(command, index):
    send_menu(Player(index))
