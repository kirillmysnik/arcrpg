from json import load

from ...resource.paths import ARCRPG_DATA_PATH


MOBS_JSON = ARCRPG_DATA_PATH / "mobs" / "mobs.json"


class MobClass:
    def __init__(self, json):
        from ..equipment.player_equipment import BotEquipment

        self.id = json['id']
        self.name = json['name']

        self.level = json['level']
        self.stat_data = {}

        for stat_id, stat_level in json.get('stats', {}).items():
            self.stat_data[stat_id] = {'level': stat_level}

        self.equipment = BotEquipment(json.get('equipment', {}))


class MobClassManager(dict):
    def __init__(self, json):
        super().__init__()

        for mob_class_json in json['mobs']:
            mob_class = MobClass(mob_class_json)
            self[mob_class.id] = mob_class


with open(MOBS_JSON, 'r') as f:
    mob_class_manager = MobClassManager(load(f))
