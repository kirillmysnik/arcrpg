from commands.server import ServerCommand
from core import echo_console

from . import secondary_class_manager_mapping
from .inventory import player_inventory_manager
from .item_instance import item_instance_manager
from .owner_types import OwnerTypes


@ServerCommand('arcrpg_create_item_instance')
def server_arcrpg_create_item_instance(command):
    try:
        secondary_class_manager_id = command[1]
        secondary_class_id = command[2]
        amount = command[3]
    except IndexError:
        echo_console("Usage: arcrpg_create_item_instance "
                     "<Secondary Class Manager ID> "
                     "<Secondary Class ID> "
                     "<amount>")
        return

    try:
        secondary_class_id = int(secondary_class_id)
    except ValueError:
        echo_console("Error: Secondary Class ID should be an integer")
        return

    try:
        amount = int(amount)
    except ValueError:
        echo_console("Error: amount should be an integer")
        return

    try:
        secondary_class_manager = secondary_class_manager_mapping[
            secondary_class_manager_id]

    except KeyError:
        echo_console("Couldn't find Secondary Class Manager (id={})".format(
            secondary_class_manager_id))

        return

    try:
        secondary_class = secondary_class_manager[secondary_class_id]
    except KeyError:
        echo_console("Couldn't find Secondary Class (id={})".format(
            secondary_class_id))

        return

    item_instance = item_instance_manager.create_new_item(secondary_class)
    item_instance.amount = amount

    item_instance.save_to_database()
    item_instance.load_from_database()

    echo_console(
        "Created item ID: {}".format(item_instance.id))


@ServerCommand('arcrpg_put_item_instance_in_player_inventory')
def server_arcrpg_put_item_instance_in_player_inventory(command):
    try:
        item_instance_id = command[1]
        owner_id = command[2]
    except IndexError:
        echo_console("Usage: arcrpg_put_item_instance_in_player_inventory "
                     "<Item Instance ID> "
                     "<Owner (inventory) ID>")
        return

    try:
        owner_id = int(owner_id)
    except ValueError:
        echo_console("Error: Owner ID should be an integer")
        return

    try:
        item_instance_id = int(item_instance_id)
    except ValueError:
        echo_console("Error: Item Instance ID should be an integer")
        return

    try:
        item_instance = item_instance_manager[item_instance_id]
    except KeyError:
        echo_console("Error: Couldn't find Item Instance (id={})".format(
            item_instance_id))

        return

    try:
        inventory = player_inventory_manager[owner_id]
    except KeyError:
        echo_console("Error: Couldn't find Owner (inventory) (id={})".format(
            owner_id))

        return

    item_instance.owner_type = OwnerTypes.PLAYER_INVENTORY
    item_instance.owner_id = owner_id

    item_instance.save_to_database()

    inventory.put_new_item(item_instance)

    echo_console("Success")


@ServerCommand('arcrpg_apply_crafted_weapon_stats')
def server_apply_crafted_weapon_stats(command):
    try:
        item_instance_id = command[1]
    except IndexError:
        echo_console("Usage: arcrpg_apply_crafted_weapon_stats "
                     "<Item Instance ID> ")
        return

    try:
        item_instance_id = int(item_instance_id)
    except ValueError:
        echo_console("Error: Item Instance ID should be an integer")
        return

    try:
        item_instance = item_instance_manager[item_instance_id]
    except KeyError:
        echo_console("Error: Couldn't find Item Instance (id={})".format(
            item_instance_id))

        return

    item_instance.secondary_class.apply_crafted_weapon_stats(item_instance)
    item_instance.save_to_database()
    item_instance.load_from_database()

    echo_console("Success")
