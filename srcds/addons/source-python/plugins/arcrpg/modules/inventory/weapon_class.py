from json import load

from paths import PLUGIN_DATA_PATH

from advanced_ts import BaseLangStrings

from ...info import info

from ...random import choice

from . import secondary_class_manager_mapping
from .quality import qualities
from .secondary_class import SecondaryClass


WEAPONS_JSON = PLUGIN_DATA_PATH / info.basename / "itemschema" / "weapons.json"

strings_weapon_classes = BaseLangStrings(
    info.basename + "/itemschema/weapon_classes")

strings_stats = BaseLangStrings(
    info.basename + "/itemschema/stats")


class Stat:
    def __init__(self, json):
        self.id = json['name']
        self.name = strings_stats[json['name']]
        self.quality = qualities[json['quality_id']]
        self.value1 = json.get('value1')
        self.value2 = json.get('value2')
        self.value3 = json.get('value3')
        self.value4 = json.get('value4')


class WeaponClass(SecondaryClass):
    manager_id = 'weapon'

    def __init__(self, json):
        super().__init__(json)

        self.min_level = json['min_level']
        self.name = strings_weapon_classes[json['name']]
        self.description = strings_weapon_classes[json['description']]
        self.equip_region = json['equip_region']
        self.entity_classname = json['entity_classname']

        self.stats = {}

        for stat_json in json.get('stats', ()):
            stat = Stat(stat_json)
            self.stats[stat.id] = stat

        self._bot_stats_number = json.get('bot_stats_number', [
            {
                'number': 0,
                'possibility': 1,
            }
        ])
        self._bot_stats_distribution = json.get('bot_stats_distribution', [])

        self._craft_stats_number = json.get('craft_stats_number', [
            {
                'number': 0,
                'possibility': 1,
            }
        ])
        self._craft_stats_distribution = json.get(
            'craft_stats_distribution', [])

    @staticmethod
    def _choose_instance_stats(number_dist_json, stat_dist_json):
        from core import echo_console
        number_distribution = []
        for number_json in number_dist_json:
            number_distribution.append((
                number_json['possibility'],
                number_json['number']
            ))

        stats_number = choice(number_distribution)
        stats = []
        free_stats_json = list(stat_dist_json)

        while free_stats_json and len(stats) < stats_number:
            stat_distribution = []
            for stat_json in free_stats_json:
                stat_distribution.append((
                    stat_json['possibility'],
                    stat_json
                ))

            stat_json = choice(stat_distribution)
            free_stats_json = list(filter(
                lambda stat_json_: stat_json_['name'] != stat_json['name'],
                free_stats_json
            ))

            stats.append(stat_json)

        return stats

    def apply_crafted_weapon_stats(self, item_instance):
        item_instance.stored_data['stats'] = self._choose_instance_stats(
            self._craft_stats_number,
            self._craft_stats_distribution
        )

    def on_instance_loaded_from_database(self, item_instance):
        item_instance['stats'] = dict(self.stats)
        for stat_json in item_instance.stored_data.get('stats', ()):
            stat = Stat(stat_json)
            item_instance['stats'][stat.id] = stat


class WeaponClassManager(dict):
    def __init__(self, json):
        super().__init__()

        for weapon_class_json in json['weapon_classes']:
            weapon_class = WeaponClass(weapon_class_json)
            self[weapon_class.id] = weapon_class


with open(WEAPONS_JSON, 'r') as f:
    weapon_class_manager = WeaponClassManager(load(f))


secondary_class_manager_mapping['weapon'] = weapon_class_manager
