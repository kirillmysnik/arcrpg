from json import load

from paths import PLUGIN_DATA_PATH

from ...info import info


ITEM_BASES_JSON = (
    PLUGIN_DATA_PATH / info.basename / "itemschema" / "primary_classes.json")


class PrimaryClass:
    def __init__(self, json):
        self.id = json['id']
        self.max_per_slot = json['max_per_slot']


class PrimaryClassManager(dict):
    def __init__(self, json):
        super().__init__()

        for primary_class_json in json['primary_classes']:
            primary_class = PrimaryClass(primary_class_json)
            self[primary_class.id] = primary_class


with open(ITEM_BASES_JSON, 'r') as f:
    primary_classes = PrimaryClassManager(load(f))
