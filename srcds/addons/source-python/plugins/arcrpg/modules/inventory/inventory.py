from json import dumps, loads

from ...internal_events import InternalEvent

from ...resource.sqlalchemy import Session

from ...models.inventory import Inventory as DB_Inventory

from .item_instance import item_instance_manager
from .owner_types import OwnerTypes


DEFAULT_MAX_SLOTS = 25


class WrongOwnerTypeError(Exception):
    pass


class DupeError(Exception):
    pass


class PlayerInventory(list):
    def __init__(self, id_):
        super().__init__()

        self.id = id_

        self.owner_type = OwnerTypes.NONE
        self.owner_id = 0

        self.max_slots = 0

    def load_from_database(self):
        db_session = Session()

        db_inventory = db_session.query(DB_Inventory).filter_by(
            id=self.id).first()

        if db_inventory is not None:
            if db_inventory.owner_type != OwnerTypes.PLAYER:
                raise WrongOwnerTypeError("Owner is not a player")

            self.owner_type = db_inventory.owner_type
            self.owner_id = db_inventory.owner_id

            self.max_slots = db_inventory.max_slots
            self.extend(loads(db_inventory.slot_data)['slots'])

        db_session.close()

    def save_to_database(self):
        db_session = Session()

        db_inventory = db_session.query(DB_Inventory).filter_by(
            id=self.id).first()

        if db_inventory is None:
            db_inventory = DB_Inventory()
            db_session.add(db_inventory)

        db_inventory.owner_type = OwnerTypes.PLAYER
        db_inventory.owner_id = self.owner_id
        db_inventory.max_slots = self.max_slots
        db_inventory.slot_data = dumps({'slots': list(list.__iter__(self))})

        db_session.commit()

        self.id = db_inventory.id

        db_session.close()

    def __iter__(self):
        for item_data in list.__iter__(self):
            if item_data['id'] == -1:
                yield None

            else:
                yield item_instance_manager[item_data['id']]

    def __getitem__(self, key):
        item_data = list.__getitem__(self, key)
        if item_data['id'] == -1:
            return None

        return item_instance_manager[item_data['id']]

    def __setitem__(self, key, item_instance):
        if item_instance is None:
            item_data = {'id': -1}
        else:
            item_data = {'id': item_instance.id}

        list.__setitem__(self, key, item_data)

    def __contains__(self, item_instance):
        for item_data in list.__iter__(self):
            if item_data['id'] == item_instance.id:
                return True

        return False

    def put_new_item(self, item_instance):
        for item_data in list.__iter__(self):
            if item_data['id'] == item_instance.id:
                return

        for i, item_data in enumerate(list.__iter__(self)):
            if item_data['id'] == -1:
                self[i] = item_instance
                break

        else:
            self.append({'id': item_instance.id})


class PlayerInventoryManager(dict):
    def __missing__(self, key):
        player_inventory = PlayerInventory(key)
        player_inventory.load_from_database()

        if player_inventory.id is None:
            raise KeyError("Couldn't pick player inventory from "
                           "database (id={})".format(key))

        self[key] = player_inventory

        return player_inventory

    def create_new_inventory(self, owner_id):
        player_inventory = PlayerInventory(None)

        player_inventory.owner_type = OwnerTypes.PLAYER
        player_inventory.owner_id = owner_id

        player_inventory.max_slots = DEFAULT_MAX_SLOTS

        player_inventory.save_to_database()

        self[player_inventory.id] = player_inventory

        return player_inventory

player_inventory_manager = PlayerInventoryManager()


@InternalEvent('rpg_player_unregistered')
def on_rpg_player_unregistered(event_var):
    rpg_player = event_var['rpg_player']

    if rpg_player.character.is_mob:
        return

    if rpg_player.character.main_inventory_id not in player_inventory_manager:
        return

    inventory = player_inventory_manager.pop(
        rpg_player.character.main_inventory_id)

    inventory.save_to_database()
