from .primary_class import primary_classes
from .quality import qualities


class SecondaryClass:
    manager_id = None

    def __init__(self, json):
        self.id = json['id']
        self.primary_class = primary_classes[json['primary_class_id']]
        self.quality = qualities[json['quality_id']]
        self.icon = json['icon']
        self.price = json['price']

        self.name = None
        self.description = None

    def on_instance_loaded_from_database(self, item_instance):
        pass
