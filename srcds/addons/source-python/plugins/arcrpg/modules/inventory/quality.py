from json import load

from paths import PLUGIN_DATA_PATH

from advanced_ts import BaseLangStrings

from ...info import info


QUALITIES_JSON = (
    PLUGIN_DATA_PATH / info.basename / "itemschema" / "qualities.json")

strings_qualities = BaseLangStrings(info.basename + "/itemschema/qualities")


class Quality:
    def __init__(self, json):
        self.id = json['id']
        self.name = strings_qualities[json['name']]
        self.color = json['color']


class QualityManager(dict):
    def __init__(self, json):
        super().__init__()

        for quality_json in json['qualities']:
            quality = Quality(quality_json)
            self[quality.id] = quality


with open(QUALITIES_JSON, 'r') as f:
    qualities = QualityManager(load(f))
