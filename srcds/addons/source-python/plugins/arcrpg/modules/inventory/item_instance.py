from collections import defaultdict
from json import dumps, loads

from ...models.item_instance import ItemInstance as DB_ItemInstance

from ...resource.sqlalchemy import Session

from . import secondary_class_manager_mapping
from .owner_types import OwnerTypes


class ItemInstance:
    def __init__(self, id_):
        super().__init__()

        self.id = id_

        self.secondary_class = None
        self.owner_type = OwnerTypes.NONE
        self.owner_id = 0
        self.amount = 0
        self.stored_data = {}

    def __eq__(self, other):
        if other is None:
            return False

        return other.id == self.id

    def load_from_database(self):
        db_session = Session()

        item = db_session.query(DB_ItemInstance).filter_by(id=self.id).first()

        if item is not None:
            self.secondary_class = secondary_class_manager_mapping[
                item.secondary_class_manager_id
            ][item.secondary_class_id]

            self.owner_type = item.owner_type
            self.owner_id = item.owner_id
            self.amount = item.amount
            self.stored_data.update(loads(item.stored_data))

            self.secondary_class.on_instance_loaded_from_database(self)

        db_session.close()

    def save_to_database(self):
        db_session = Session()

        item = db_session.query(DB_ItemInstance).filter_by(id=self.id).first()

        if item is None:
            item = DB_ItemInstance()

            db_session.add(item)

        item.secondary_class_manager_id = self.secondary_class.manager_id
        item.secondary_class_id = self.secondary_class.id

        item.owner_type = self.owner_type
        item.owner_id = self.owner_id
        item.amount = self.amount
        item.stored_data = dumps(self.stored_data)

        db_session.commit()

        self.id = item.id

        db_session.close()


class ItemInstanceManager(defaultdict):
    def __missing__(self, key):
        item_instance = ItemInstance(key)
        item_instance.load_from_database()

        if item_instance.secondary_class is None:
            raise KeyError("Couldn't pick item instance from "
                           "database (id={})".format(key))

        self[key] = item_instance

        return item_instance

    def create_new_item(self, secondary_class):
        item_instance = ItemInstance(None)

        item_instance.secondary_class = secondary_class

        item_instance.save_to_database()

        self[item_instance.id] = item_instance

        return item_instance

item_instance_manager = ItemInstanceManager()
