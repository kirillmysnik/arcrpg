from events import Event

from ...classes.rpgplayer import rpg_player_manager, tell
from ...resource.config_cvars import config_manager
from ...resource.strings import build_module_strings

from . import new_rate_cvar, rate_manager


strings_module = build_module_strings('exp.kill')

cvar_exp_on_kill = new_rate_cvar('on_kill', 100, "")


@Event('player_death')
def on_player_death(game_event):
    userid = game_event['userid']
    attackerid = game_event['attacker']

    if attackerid in (0, userid):
        return

    rpg_attacker = rpg_player_manager.get_by_userid(attackerid)
    if rpg_attacker.is_mob:
        return

    rpg_player = rpg_player_manager.get_by_userid(userid)

    exp = int(rate_manager.get_rate(rpg_attacker) * cvar_exp_on_kill.get_int())

    if config_manager['announce_received_exp_in_chat']:
        tell(rpg_attacker, strings_module['exp rewarded'].tokenize(
            exp=exp,
            victim=rpg_player.player.name,
        ))

    rpg_attacker.receive_exp(exp)
