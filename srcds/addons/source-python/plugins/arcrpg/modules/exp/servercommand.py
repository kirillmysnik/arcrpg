from commands.server import ServerCommand
from core import echo_console

from ...classes.rpgplayer import rpg_player_manager, tell
from ...resource.strings import build_module_strings


strings_module = build_module_strings('exp.servercommand')


@ServerCommand('arcrpg_cheat_add_exp')
def server_arcrpg_cheat_add_exp(command):
    try:
        userid = command[1]
        exp = command[2]
    except IndexError:
        echo_console("Usage: arcrpg_cheat_add_exp <userid> <exp amount>")
        return

    try:
        userid = int(userid)
    except ValueError:
        echo_console("Error: userid should be an integer")
        return

    try:
        exp = max(0, int(exp))
    except ValueError:
        echo_console("Error: exp should be an integer")
        return

    try:
        rpg_player = rpg_player_manager.get_by_userid(userid)
    except KeyError:
        echo_console("Couldn't find RPGPlayer (userid={})".format(userid))
        return

    tell(rpg_player, strings_module['exp rewarded'].tokenize(
        exp=exp,
    ))

    rpg_player.receive_exp(exp)
    echo_console(
        "Rewarded {} exp to the player (userid={})".format(exp, userid))
