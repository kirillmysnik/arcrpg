from config.manager import ConfigManager
from cvars.flags import ConVarFlags

from ...info import info


CONFIG_FILE = info.basename + '/' + info.basename + '_rates'


class RateManager:
    def __init__(self):
        self.rate = 1.0

    def get_rate(self, target):
        return self.rate

rate_manager = RateManager()

rate_config = ConfigManager(CONFIG_FILE, cvar_prefix='arcrpg_rates_')


def new_rate_cvar(name, default, description):
    return rate_config.cvar(
        name=name,
        default=default,
        description=description,
        flags=ConVarFlags.NOTIFY,
    )


import os

from .. import parse_modules

__all__ = parse_modules(os.path.dirname(__file__))

from . import *


rate_config.write()
rate_config.execute()
