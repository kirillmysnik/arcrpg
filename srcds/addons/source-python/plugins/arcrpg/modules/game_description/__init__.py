from engines.server import server_game_dll

from memory import get_virtual_function
from memory.hooks import PreHook

from ...info import info


ARCRPG_DESCRIPTION = "{} {}".format(info.name, info.version)


@PreHook(get_virtual_function(server_game_dll, 'GetGameDescription'))
def pre_get_game_description(args):
    return ARCRPG_DESCRIPTION
