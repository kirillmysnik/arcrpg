from motdplayer.plugin_instance import PluginInstance

from ...info import info


plugin_instance = PluginInstance(info.basename)
