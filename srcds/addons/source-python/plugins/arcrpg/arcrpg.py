from events import Event
from filters.players import PlayerIter
from listeners import OnClientActive, OnClientDisconnect, OnLevelShutdown
from paths import CFG_PATH
from players.entity import Player
from players.helpers import index_from_userid
from players.teams import teams_by_name
from stringtables.downloads import Downloadables

from .info import info

from .internal_events import InternalEvent


DOWNLOADLIST = CFG_PATH / info.basename / "downloadlists" / "main.txt"


def load_downloadables(filepath):
    downloadables = Downloadables()

    with open(filepath) as f:
        for line in f:
            line = line.strip()
            if not line:
                continue
            downloadables.add(line)

    return downloadables

downloadables_global = load_downloadables(DOWNLOADLIST)


class PlayerManager(dict):
    def __setitem__(self, key, value):
        dict.__setitem__(self, key, value)
        InternalEvent.fire('player_registered', player=value)

    def __delitem__(self, key):
        InternalEvent.fire('player_unregistered', player=self[key])
        dict.__delitem__(self, key)

    def get_by_userid(self, userid):
        return self[index_from_userid(userid)]

player_manager = PlayerManager()


def load():
    for player in PlayerIter():
        player_manager[player.index] = player

    InternalEvent.fire('load')


def unload():
    InternalEvent.fire('unload')

    for index in tuple(player_manager.keys()):
        del player_manager[index]


@OnClientActive
def listener_on_client_active(index):
    player_manager[index] = Player(index)


@OnClientDisconnect
def listener_on_client_disconnect(index):
    if index in player_manager:
        del player_manager[index]


@OnLevelShutdown
def listener_on_level_shutdown():
    for index in tuple(player_manager.keys()):
        del player_manager[index]


@Event('player_spawn')
def on_player_spawn(game_event):
    player = player_manager.get_by_userid(game_event['userid'])
    if player.team in (teams_by_name['t'], teams_by_name['ct']):
        InternalEvent.fire(
            'player_respawn',
            player=player,
            game_event=game_event,
        )


from . import modules

from . import models
from .resource.sqlalchemy import Base, engine
Base.metadata.create_all(engine)
