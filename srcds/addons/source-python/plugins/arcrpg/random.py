from random import randint


def choice(distribution):
    """
    Example #1.
    Sample distribution
    [
        (5, "banana"),
        (2, "apple"),
        (2, "watermelon"),
        (1, "orange"),
    ]

    has the following chances:
        banana - 50%
        apple - 20%
        watermelon - 20%
        orange - 10%

    Example #2.
    Sample distribution
    [
        (1, "banana"),
        (1, "apple"),
        (1, "watermelon"),
    ]

    has the following chances:
        banana - 33.3%
        apple - 33.3%
        watermelon - 33.3%

    """
    sum_ = 0
    boundaries = []
    for option_possibility, option_value in distribution:
        sum_ += option_possibility
        boundaries.append(sum_)

    x = randint(1, sum_)

    for i, boundary in enumerate(boundaries):
        if x <= boundary:
            option_possibility, option_value = distribution[i]
            return option_value
