from flask import render_template

from . import app, plugin_instance


@app.route(plugin_instance.get_base_authed_route('index'))
def route_index(steamid, auth_method, auth_token, session_id):
    context = {
        'server_id': plugin_instance.server_id,
        'stylesheets': ('main', 'index'),
        'scripts': ('dom', 'index'),
        'steamid': steamid,
        'auth_method': auth_method,
        'auth_token': auth_token,
        'session_id': session_id,
    }
    return render_template('arcrpg/route_index.html', **context)


@plugin_instance.json_authed_request('json-index')
def json_index(data_exchanger, json_data):
    if json_data['action'] == "init":
        return data_exchanger.exchange({
            'action': "init",
        })

    if json_data['action'] == "apply-stats-changes":
        return data_exchanger.exchange({
            'action': "apply-stats-changes",
            'spare_stats': json_data['spare_stats'],
            'stats_data': json_data['stats_data'],
        })

    return None


@app.route(plugin_instance.get_base_authed_route('inventory'))
def route_inventory(steamid, auth_method, auth_token, session_id):
    context = {
        'server_id': plugin_instance.server_id,
        'stylesheets': ('main', 'inventory'),
        'scripts': ('dom', 'inventory'),
        'steamid': steamid,
        'auth_method': auth_method,
        'auth_token': auth_token,
        'session_id': session_id,
    }
    return render_template('arcrpg/route_inventory.html', **context)


@plugin_instance.json_authed_request('json-inventory')
def json_inventory(data_exchanger, json_data):
    if json_data['action'] == "init":
        return data_exchanger.exchange({
            'action': "init",
        })

    if json_data['action'] == "equip":
        return data_exchanger.exchange({
            'action': "equip",
            'item_instance_id': json_data['item_instance_id'],
        })

    return None


@app.route(plugin_instance.get_base_authed_route('charselect'))
def route_charselect(steamid, auth_method, auth_token, session_id):
    context = {
        'server_id': plugin_instance.server_id,
        'stylesheets': ('main', 'charselect'),
        'scripts': ('dom', 'charselect'),
        'steamid': steamid,
        'auth_method': auth_method,
        'auth_token': auth_token,
        'session_id': session_id,
    }
    return render_template('arcrpg/route_charselect.html', **context)


@plugin_instance.json_authed_request('json-charselect')
def json_charselect(data_exchanger, json_data):
    if json_data['action'] == "init":
        return data_exchanger.exchange({
            'action': "init",
        })

    if json_data['action'] == "select":
        return data_exchanger.exchange({
            'action': "select",
            'char_id': json_data['char_id'],
        })

    if json_data['action'] == "create":
        return data_exchanger.exchange({
            'action': "create",
            'name': json_data['name'],
        })
