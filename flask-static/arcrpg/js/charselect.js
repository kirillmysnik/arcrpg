APP['charSelect'] = {};
APP['charSelect'].CharSelect = function (motdPlayer) {
    var charSelect = this;

    var nodes = {};

    var renderCharacters = function (characters) {

    };

    motdPlayer.retarget('json-charselect', function () {
        motdPlayer.post({
            action: "init",
        }, function (data) {
            renderCharacters(data['characters']);
        }, function (error) {
            alert("Initialization error\n" + error);
        });
    }, function (error) {
        alert("Retargeting error\n" + error);
    });
}
