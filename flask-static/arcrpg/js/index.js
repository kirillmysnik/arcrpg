APP['index'] = {};
APP['index'].Index = function (motdPlayer) {
    var index = this;

    var nodes = {};
    nodes['body'] = document.getElementsByTagName('body')[0];
    nodes['title-level'] = document.getElementById('title-level');
    nodes['spare-stats-counter'] = document.getElementById('spare-stats-counter');
    nodes['class-table'] = document.getElementById('class-table');
    nodes['upgrade-table'] = document.getElementById('upgrade-table');
    nodes['button-save-changes'] = document.getElementById('button-save-changes');

    var statLevels = {};
    var spareStats = 0;

    nodes['button-save-changes'].addEventListener('click', function (e) {
        var statsData = {};
        for (var statName in statLevels) {
            statsData[statName] = statLevels[statName].level;
        }
        motdPlayer.post({
            action: "apply-stats-changes",
            spare_stats: spareStats,
            stats_data: statsData
        }, function (data) {
            renderCharData(data['chardata']);
        });
    });

    var renderCharData = function (data) {
        // logic
        statLevels = {};
        spareStats = data['spare_stats'];

        // title
        clearNode(nodes['title-level']);
        nodes['title-level'].appendChild(document.createTextNode(data.level));

        // spare stats counter
        clearNode(nodes['spare-stats-counter']);
        nodes['spare-stats-counter'].appendChild(document.createTextNode(data['spare_stats']));
        if (data['spare_stats'] > 0)
            nodes['spare-stats-counter'].parentNode.classList.add('spare-stats-available');
        else
            nodes['spare-stats-counter'].parentNode.classList.remove('spare-stats-available');

        // class & upgrade tables
        clearNode(nodes['class-table']);
        clearNode(nodes['upgrade-table']);

        var tr, td;

        tr = nodes['class-table'].appendChild(document.createElement('tr'));
        td = tr.appendChild(document.createElement('td'));
        td.appendChild(document.createTextNode("level: "));
        td.classList.add('stat-name');
        td = tr.appendChild(document.createElement('td'));
        td.appendChild(document.createTextNode(data.level));
        td.classList.add('stat-value');

        tr = nodes['class-table'].appendChild(document.createElement('tr'));
        td = tr.appendChild(document.createElement('td'));
        td.appendChild(document.createTextNode("exp: "));
        td.classList.add('stat-name');
        td = tr.appendChild(document.createElement('td'));
        td.appendChild(document.createTextNode(data.exp + "/" + data['levelup_exp']));
        td.classList.add('stat-value');

        for (var i=0; i < data.stats.length; i++) {
            stat = data.stats[i];

            (function (stat) {
                var tr, td, input;
                // logic
                statLevels[stat.id] = {
                    minLevel: stat.level,
                    level: stat.level
                };
                // class table
                tr = nodes['class-table'].appendChild(document.createElement('tr'));

                td = tr.appendChild(document.createElement('td'));
                td.appendChild(document.createTextNode(stat['class_entry_name'] + ": "));
                td.classList.add('stat-name');

                td = tr.appendChild(document.createElement('td'));
                td.appendChild(document.createTextNode(stat['class_entry_value']));
                td.classList.add('stat-value');

                // upgrade table
                tr = nodes['upgrade-table'].appendChild(document.createElement('tr'));

                td = tr.appendChild(document.createElement('td'));
                td.appendChild(document.createTextNode(stat['name']));
                td.classList.add('stat-name');

                td = tr.appendChild(document.createElement('td'));
                input = td.appendChild(document.createElement('input'));
                input.type = 'button';
                input.value = "-";
                input.addEventListener('click', function (e) {
                    tryDecreaseStat(stat.id);
                })

                td = tr.appendChild(document.createElement('td'));
                td.appendChild(document.createTextNode(stat['level']));
                td.classList.add('stat-level');
                statLevels[stat.id]['node'] = td;

                td = tr.appendChild(document.createElement('td'));
                input = td.appendChild(document.createElement('input'));
                input.type = 'button';
                input.value = "+";
                input.addEventListener('click', function (e) {
                    tryIncreaseStat(stat.id);
                })
            })(stat);
        }
    };

    var tryIncreaseStat = function (statId) {
        if (spareStats <= 0)
            return;

        spareStats -= 1;
        statLevels[statId].level += 1;

        clearNode(nodes['spare-stats-counter']);
        nodes['spare-stats-counter'].appendChild(document.createTextNode(spareStats));
        nodes['spare-stats-counter'].classList.remove('spare-stats-available');

        clearNode(statLevels[statId].node);
        statLevels[statId].node.appendChild(document.createTextNode(statLevels[statId].level));
    };

    var tryDecreaseStat = function (statId) {
        if (statLevels[statId].level <= statLevels[statId].minLevel)
            return;

        spareStats += 1;
        statLevels[statId].level -= 1;

        clearNode(nodes['spare-stats-counter']);
        nodes['spare-stats-counter'].appendChild(document.createTextNode(spareStats));
        nodes['spare-stats-counter'].classList.remove('spare-stats-available');

        clearNode(statLevels[statId].node);
        statLevels[statId].node.appendChild(document.createTextNode(statLevels[statId].level));
    };

    motdPlayer.retarget('json-index', function () {
        motdPlayer.post({
            action: "init",
        }, function (data) {
            renderCharData(data['chardata']);
        }, function (error) {
            alert("Initialization error\n" + error);
        });
    }, function (error) {
        alert("Retargeting error\n" + error);
    });
}