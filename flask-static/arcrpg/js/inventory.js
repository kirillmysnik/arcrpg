APP['inventory'] = {};
APP['inventory'].Inventory = function (motdPlayer) {
    var inventory = this;

    var nodes = {};
    nodes['body'] = document.getElementsByTagName('body')[0];
    nodes['inventory'] = document.getElementById('inventory');
    nodes['item-stats'] = document.getElementById('item-stats');

    nodes['equipment-primary'] = document.getElementById('equipment-primary');
    nodes['equipment-secondary'] = document.getElementById('equipment-secondary');
    nodes['equipment-knife'] = document.getElementById('equipment-knife');
    nodes['equipment-hegrenade'] = document.getElementById('equipment-hegrenade');
    nodes['equipment-flashbang'] = document.getElementById('equipment-flashbang');
    nodes['equipment-smokegrenade'] = document.getElementById('equipment-smokegrenade');

    var renderInventory = function (data) {
        clearNode(nodes['inventory']);

        var currentRow = null;

        var insertNewRow = function () {
            if (currentRow) {
                var clearer = currentRow.appendChild(document.createElement('div'));
                clearer.classList.add('clear');
            }

            currentRow = nodes['inventory'].appendChild(document.createElement('div'));
            currentRow.classList.add('inventory-row');
        };

        var fillItemNode = function (nodeItem, item) {
            nodeItem.classList.add('inventory-item');

            if (item.empty)
                nodeItem.classList.add('empty');

            else {
                nodeItem.classList.remove('empty');

                nodeItem.style.backgroundImage = "url(/static/arcrpg/img/items/" + item['icon'] + ")";
                nodeItem.style.borderColor = item['quality_color'];

                if (item['equipped']) {
                    var span = nodeItem.appendChild(document.createElement('span'));
                    span.appendChild(document.createTextNode("equipped"));
                    span.classList.add('equipped');
                }

                nodeItem.addEventListener('click', function (e) {
                    motdPlayer.post({
                        action: "equip",
                        item_instance_id: item['id'],
                    }, function (data) {
                        renderInventory(data['inventory']);
                    }, function (error) {
                        alert("Couldn't equip/unequip the item\n" + error);
                    });
                });

                nodeItem.addEventListener('mouseover', function (e) {
                    clearNode(nodes['item-stats']);

                    var span = nodes['item-stats'].appendChild(document.createElement('span'));
                    span.appendChild(document.createTextNode(item['name']))
                    span.classList.add('name');
                    span.style.color = item['quality_color'];

                    nodes['item-stats'].appendChild(document.createElement('br'));
                    nodes['item-stats'].appendChild(document.createElement('br'));

                    span = nodes['item-stats'].appendChild(document.createElement('span'));
                    span.appendChild(document.createTextNode("Quality: "));

                    var span2 = span.appendChild(document.createElement('span'));
                    span2.appendChild(document.createTextNode(item['quality_name']));
                    span2.classList.add('quality-box');
                    span2.style.backgroundColor = item['quality_color'];
                    span2.style.borderColor = item['quality_color'];

                    span.classList.add('quality');

                    nodes['item-stats'].appendChild(document.createElement('br'));

                    span = nodes['item-stats'].appendChild(document.createElement('span'));
                    span.appendChild(document.createTextNode(item['description']));
                    span.classList.add('description');

                    nodes['item-stats'].appendChild(document.createElement('br'));

                    var ul = nodes['item-stats'].appendChild(document.createElement('ul'));
                    for (var i=0; i<item['stats'].length; i++) {
                        var stat = item['stats'][i];

                        var li = ul.appendChild(document.createElement('li'));
                        li.appendChild(document.createTextNode(stat['text']));
                        li.style.color = stat['quality_color'];
                    }

                    nodes['item-stats'].classList.add('visible');
                });

                nodeItem.addEventListener('mouseout', function (e) {
                    nodes['item-stats'].classList.remove('visible');
                });
            }
        };

        var item, nodeItem;
        for (var i=0; i<data['items'].length; i++) {
            (function(item) {
                if (i % data['row_length'] == 0)
                    insertNewRow();

                nodeItem = currentRow.appendChild(document.createElement('div'));

                fillItemNode(nodeItem, item);

            })(data['items'][i]);
        }

        if (currentRow) {
            var clearer = currentRow.appendChild(document.createElement('div'));
            clearer.classList.add('clear');
        }

        clearNode(nodes['equipment-primary']);
        nodeItem = nodes['equipment-primary'].appendChild(document.createElement('div'));
        fillItemNode(nodeItem, data['equipment']['primary']);

        clearNode(nodes['equipment-secondary']);
        nodeItem = nodes['equipment-secondary'].appendChild(document.createElement('div'));
        fillItemNode(nodeItem, data['equipment']['secondary']);

        clearNode(nodes['equipment-knife']);
        nodeItem = nodes['equipment-knife'].appendChild(document.createElement('div'));
        fillItemNode(nodeItem, data['equipment']['knife']);

        clearNode(nodes['equipment-hegrenade']);
        nodeItem = nodes['equipment-hegrenade'].appendChild(document.createElement('div'));
        fillItemNode(nodeItem, data['equipment']['hegrenade']);

        clearNode(nodes['equipment-flashbang']);
        nodeItem = nodes['equipment-flashbang'].appendChild(document.createElement('div'));
        fillItemNode(nodeItem, data['equipment']['flashbang']);

        clearNode(nodes['equipment-smokegrenade']);
        nodeItem = nodes['equipment-smokegrenade'].appendChild(document.createElement('div'));
        fillItemNode(nodeItem, data['equipment']['smokegrenade']);
    };

    document.addEventListener('mousemove', function(e) {
        nodes['item-stats'].style.top = e.screenY + 2 + 'px';
        nodes['item-stats'].style.left = e.screenX + 15 + 'px';
    });

    motdPlayer.retarget('json-inventory', function () {
        motdPlayer.post({
            action: "init",
        }, function (data) {
            renderInventory(data['inventory']);
        }, function (error) {
            alert("Initialization error\n" + error);
        });
    }, function (error) {
        alert("Retargeting error\n" + error);
    });
}